# Surveillant des notes

Bot Discord avertissant à la publication d'une nouvelle note sur https://rvn.grenet.fr/inp/

## Prérequis

* Python3 : ```sudo apt install python3 python3-pip```
* Mechanize : ```python3 -m pip install mechanize```
* Discord.py : ```python3 -m pip install discord.py```

## Configuration

Renommez le fichier de configuration :

```sh
mv config_example.ini config.ini
```

Modifiez ensuite ce fichier pour ajouter vos identifiants AGALAN, le token, le channel et l'id du rôle utilisé par le bot et éventuellement le nom des fichiers et leur répertoire d'enregistrement.

Listez ensuite l'ensemble de vos matières. Des exemples de configuration des matières sont disponibles ici : [CONFIG.md](CONFIG.md).

## Démarrage simple

```sh
python3 surveillant_des_notes.py
```

## Automatisation

Tout d'abord il faut rendre le fichier éxecutable pour l'utilisateur :

```sh
chmod u+x surveillant_des_notes.py
```

Ensuite, modifiez le fichier ```sdnotes.service``` en y ajoutant votre nom d'utilisateur, nom du groupe, et adresse de l'éxecutable.

Déplacez le ensuite dans le dossier suivant et exécutez les commandes suivantes (avec les droits root) :

```sh
sudo mv sdnotes.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable sdnotes.service
sudo systemctl start sdnotes.service
```

Vous pouvez vérifier son bon fonctionnement en exécutant la commande suivante :

```sh
sudo systemctl status sdnotes.service
```

